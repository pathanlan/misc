set anItem to ("SSD:Users:patrick.hanlan:Music:The Legend of Zelda_ Breath of the Wild Soundtrack:001. Main Theme - The Legend of Zelda Breath of the Wild OST.mp3" as alias)

tell application "iTunes"
	set everyTrack to every file track of playlist "Zelda"
	repeat with aTrack in everyTrack
		set aResult to (find text "(?<=(\\s-\\s)).*" in (get name of aTrack) with regexp)
		set trackTitle to (matchResult of aResult)
		set aResult to (find text "((\\d){3})" in the (get name of aTrack) with regexp)
		set trackNumber to (matchResult of aResult as number)
		
		set name of aTrack to trackTitle
		set track number of aTrack to trackNumber
	end repeat
end tell